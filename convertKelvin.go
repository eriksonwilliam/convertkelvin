package main

import "fmt"

const ebulicaoKelvin = 373.15

func main() {
	celsius := ebulicaoKelvin - 273.15

	fmt.Printf("A temperatura de ebulição em Kelvin é: %g e a temperatura em Celsius é: %g", ebulicaoKelvin, celsius)
}
